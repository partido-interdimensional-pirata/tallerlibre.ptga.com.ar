---
title: Taller Libre de electrónica
author: f@sutty.nl
layout: post
---

[P.T.G.A](https://ptga.com.ar/) es una cooperativa que se dedica a la
prestación de servicios integrales de telecomunicaciones. Tenemos más de
una década de trabajo con proyectos comunicacionales y formación de
medios alternativos en el marco de la [RNMA](http://rnma.org.ar),
llegando a construir un modelo propio de Transmisor de TV Digital.
Entendemos la comunicación como un derecho humano y vemos como
indispensable la democratización de los medios de comunicación para
generar una mirada crítica y democrática de la realidad social.

Uno de nuestros objetivos es la formación técnica de compañeres. Para
eso abrimos el área "Taller libre", un espacio en donde se promueve la
formación colectiva, abierta y participativa con una perspectiva
feminista en la apropiación de conocimientos técnicos. Se fomentan
distintos proyectos como el armado de transmisores, la auto-reparación
de dispositivos e incluso la construcción de biodigestores y otras
fuentes alternativas de energía.
