# Elimina los artículos marcados como borradores
Jekyll::Hooks.register :site, :post_read do |site|
  site.posts.docs.delete_if do |post|
    d = [post.data['incomplete'],post.data['draft']].any?
    Jekyll.logger.info "Borrador: #{post.data['title']}" if d

    d
  end
end
